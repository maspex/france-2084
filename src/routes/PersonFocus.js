import React, { useState, useEffect } from 'react'
import { Item, Grid, Icon, Container } from 'semantic-ui-react'
import { useRouteMatch, useParams } from "react-router-dom";
import { getUser } from "../api";
import PeopleList from '../routes/PeopleList';
import PurchasesList from '../components/PurchasesList';
import MessagesList from '../components/MessagesList';

export default function PersonFocus(props) {
  const match = useRouteMatch();
  const {personGUID} = useParams();
  const [person, setPerson] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUser(personGUID)
    .then(res => {
      setPerson(res.data);
      setLoading(false);
    })
    //TODO catch
  }, []);

  if (loading)
    return <Container textAlign='center'><Icon size="massive" name="spinner" center loading/></Container>

  if (!person) {
    return <h1>no user found with this guid (TODO: proper error page)</h1>
  }

  //fix id => _id
  if (person.friends)
    person.friends.forEach(friend => friend._id = friend.id)

  return <Item>
    <Item.Image size='tiny' src={person.picture} />

    <Item.Content>
      <Item.Header>{person.name.first} {person.name.last}</Item.Header>
      <Item.Meta>
        <span className='eye-color'>{person.eyeColor}</span>
        <span className='age'>{person.age}</span>
      </Item.Meta>
      <Item.Description>{person.about}</Item.Description>

      <Grid columns={2} divided>
        <Grid.Column>
          {person.purchases && 
            <PurchasesList purchases={person.purchases} />
            || <div>no purchases recorded</div>
          }
        </Grid.Column>
        <Grid.Column>
          {person.messages && 
            <MessagesList messages={ person.messages } />
            || <div>no messages recorded</div>
          }
        </Grid.Column>
      </Grid>

      <textarea style={{width:"100%", height: "600px"}} value={JSON.stringify(person, null, 4)} readOnly></textarea>

      { person.friends &&
          <PeopleList people={person.friends}/>
      }

    </Item.Content>
  </Item>
}