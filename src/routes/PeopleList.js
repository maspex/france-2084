import React, { useState } from 'react'
import { List, Icon } from 'semantic-ui-react'
import PeopleItem from '../components/PeopleItem'

const recursiveFind = (val, search, _hits = []) => {
    switch (typeof val) {
        case "object":
            if (Array.isArray(val)) {
                return [..._hits, ...val.map(e => recursiveFind(e, search, _hits))].flat();
            }
            else {
                return [..._hits, ...Object.keys(val).map(key => recursiveFind(val[key], search, _hits))].flat();
            }
            break;
        case "number":
        case "string":
        case "boolean":
            if (val.toString().includes(search)) {
                return _hits.concat(val.toString()).flat();
            }
            break;
        default:
            console.warn("unhandled recursive search type:", typeof val);
            break;
    }

    return _hits.flat();
}

const search = (person, searchQuery) => {
    if (searchQuery.length > 0) {
        const searchRes = recursiveFind(person, searchQuery);
        return searchRes.length > 0;
    }
    return true;
}

export default function PeopleList({people}) {
    const [searchQuery, setSearchQuery] = useState("");

    return <>
        <div className="ui search input icon fluid">
            <input className="prompt" value={searchQuery} onChange={e => setSearchQuery(e.target.value)} />
            <Icon name="search" />
        </div>
        <List size="massive" divided verticalAlign='middle'>
            {
                people
                .filter(person => search(person, searchQuery))
                .map((person) => <PeopleItem key={person._id} person={person} /> )
            }
        </List>
    </>
}