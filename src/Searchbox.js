import React from 'react';
import { Input } from 'semantic-ui-react';

//Using destructuring here, ({...}) allows me to grab the props object and grab it's properties
const Searchbox = ({ searchfield, searchChange }) => {
    return (
        <div className='pa2'>
            <Input
            // className='pa3 ba b--green bg-lightest-blue' // tachyon styling
            action='Search'
            placeholder='search robots'
            onChange={searchChange}
            />
        </div>
    ); //onChange is an event listener listening to anytime the input changes
}

export default Searchbox;