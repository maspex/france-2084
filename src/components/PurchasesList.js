import React, { useState } from 'react'
import { Table, Menu, Icon } from 'semantic-ui-react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default function PurchasesList({purchases}) {
  const [filterAmount, setFilterAmount] = useState(0);
  const [filterDate, setFilterDate] = useState(new Date().setFullYear(2015));

  const totalSpent = purchases.reduce((acc, curr) => acc + parseFloat(curr.amount), 0);
  return <Table singleLine>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Date</Table.HeaderCell>
        <Table.HeaderCell>Amount (total: {<span style={{color:totalSpent > 30000 ? "red" : "green"}}>{totalSpent}€</span>})</Table.HeaderCell>
      </Table.Row>
      <Table.Row>
        <Table.HeaderCell>
          <DatePicker selected={filterDate} onChange={setFilterDate} />
        </Table.HeaderCell>
        <Table.HeaderCell>
          <input value={filterAmount} onChange={(e) => setFilterAmount(parseFloat(e.target.value) || 0)} />
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {
      purchases
      .filter(purchase => parseFloat(purchase.amount) >= filterAmount)
      .filter(purchase => new Date(purchase.date) >= filterDate)
      .map((purchase, index) =>
        <Table.Row key={index}>
          <Table.Cell>
              {purchase.date}
          </Table.Cell>
          <Table.Cell>
              {parseFloat(purchase.amount)}€
          </Table.Cell>
        </Table.Row>
      )}
    </Table.Body>
  </Table>
}