import React from 'react'
import { Table } from 'semantic-ui-react'


export default function MessagesList({messages}) {
  // const 


   return <Table fixed>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Source</Table.HeaderCell>
        <Table.HeaderCell>Date</Table.HeaderCell>
        <Table.HeaderCell>Content</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {
        messages
        .map((message, index) =>
          <Table.Row key={index}>
            <Table.Cell>
                {message.source}
            </Table.Cell>
            <Table.Cell>
                {message.date}
            </Table.Cell>
            <Table.Cell>
                {message.content}
            </Table.Cell>
          </Table.Row>
        )}
    </Table.Body>
  </Table>
}

