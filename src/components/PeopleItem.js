import React from 'react'
import { List, Image } from 'semantic-ui-react'
import { Link } from "react-router-dom";

export default function PeopleItem({person}) {
    const totalBought = person.purchases ? person.purchases.reduce((acc, curr) => acc + parseFloat(curr.amount), 0) : 0;

    return <List.Item>
        <Image avatar size="tiny" src={person.picture} />
        <List.Content>
            <List.Header as={Link} to={'/person/' + person.guid}>
                {person.name.first} {person.name.last} {totalBought >= 30000 ? `(! total purchases ${totalBought} )` : ""}
            </List.Header>
            <List.Description>
                {person.guid}
            </List.Description>
        </List.Content>
    </List.Item>
}
